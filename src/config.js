const config = {
  limit: 10,
  timeout: 10000,
  api: {
    url: 'http://api.openweathermap.org/data/2.5/weather',
    key: 'b76d41bd6319a0921579b104f131fda2'
  }
};

export default config;
