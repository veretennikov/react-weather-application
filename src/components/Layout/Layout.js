import React from 'react';
import './Layout.css';

const Layout = (props) => (
  <div className="Layout">
    <div className="Layout__row">
      <div className="Layout__cell">
        <div className="Layout__container">
          {props.children}
        </div>
      </div>
    </div>
  </div>
);

export default Layout;
