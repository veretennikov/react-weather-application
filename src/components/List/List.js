import React from 'react';
import Card from '../Card';
import './List.css';

const List = (props) => (
  <ul className="List">
    {props.items && props.items.map((item, index) =>
      <li className="List__item" key={item.id}>
        <Card
          id={item.id}
          title={item.title}
          notice={item.notice}
          value={`${item.value}°C`}
          index={index}
          onRemove={(i) => props.onRemove(i)}
        />
      </li>
    )}
  </ul>
);

export default List;
