import React from 'react';
import './Grid.css';

const Grid = (props) => (
  <div className="Grid">
    <div className="Grid__item">
      {props.topbar}
    </div>

    <div className="Grid__item">
      {props.content}
    </div>

    <div className="Grid__item">
      {props.bottombar}
    </div>
  </div>
);

export default Grid;

