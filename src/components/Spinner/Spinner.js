import React from 'react';
import spinner from './images/spinner.png';
import './Spinner.css';

const Spinner = () => (
  <img className="Spinner" src={spinner} alt="...Loading" width="50" height="50" />
);

export default Spinner;
