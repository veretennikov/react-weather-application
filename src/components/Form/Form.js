import React from 'react';
import './Form.css';

function handleSubmit(event, handler) {
  event.preventDefault();
  handler();
}

const Form = (props) => (
  <form className="Form" onSubmit={(event) => handleSubmit(event, props.onSubmit)}>
    <input
      className="Form__input"
      placeholder="Enter city"
      value={props.value}
      onChange={(event) => props.onChange(event.target.value)}
    />
    <button className="Form__button">add</button>
  </form>
);

export default Form;
