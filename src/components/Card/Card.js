import React from 'react';
import './Card.css';

const Card = (props) => (
  <div className="Card" onClick={() => props.onRemove(props.index)}>
    <div className="Card__wrapper">
      <div className="Card__section">
        <div className="Card__title">{props.title}</div>

        {props.notice &&
          <div className="Card__notice">{props.notice}</div>
        }
      </div>

        <div className="Card__section">
          <div className="Card__value">{props.value}</div>
          <div className="Card__label">remove</div>
        </div>
    </div>
  </div>
);

export default Card;
