import React from 'react';
import logo from './images/logo.png';
import './Header.css';

const Header = (props) => (
  <div className="Header">
    <img className="Header__icon" src={logo} alt="" width="64" height="64" />
    <h1 className="Header__title">React Weather Application</h1>
  </div>
);

export default Header;
