import React from 'react';
import App from '../App';
import './Root.css';

const Root = () => (
  <App />
);

export default Root;
