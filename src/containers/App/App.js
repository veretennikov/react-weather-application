import React, { Component } from 'react';
import DataService from '../../services/DataService';
import CacheService from '../../services/CacheService';
import Layout from '../../components/Layout';
import Grid from '../../components/Grid';
import Header from '../../components/Header';
import Spinner from '../../components/Spinner';
import List from '../../components/List';
import Form from '../../components/Form';
import config from '../../config';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPending: false,
      list: [],
      input: '',
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleFormChange = this.handleFormChange.bind(this);
    this.handleRemoveClick = this.handleRemoveClick.bind(this);
    this.handleFailedGeolocation = this.handleFailedGeolocation.bind(this);
    this.handleSuccessGeolocation = this.handleSuccessGeolocation.bind(this);
  }

  async handleFormSubmit() {
    const list = this.state.list;
    const input = this.state.input.trim();

    this.setState({ isPending: true });

    if (input.length > 0 && !this.checkDuplicate(input, list)) {
      const data = await DataService.getDataByCityName(input);
      if (data) {
        list.push(data);
      }
    }

    this.setState({
      isPending: false,
      input: '',
      list,
    }, this.syncList);
  }

  syncList() {
    CacheService.setList(this.state.list);
  }

  checkDuplicate(input, list) {
    const keys = list.map((item) => item.title);
    return keys.includes(input);
  }

  handleFormChange(value) {
    this.setState({
      input: value,
    });
  }

  handleRemoveClick(index) {
    const list = this.state.list;
    list.splice(index, 1);
    this.setState({
      list,
    }, this.syncList);
  }

  setListFromCooridnates() {
    if ('geolocation' in navigator) {
      this.setState({
        isPending: true,
      });

      navigator.geolocation.getCurrentPosition(
        this.handleSuccessGeolocation,
        this.handleFailedGeolocation, {
          timeout: config.timeout,
        }
      );
    }
  }

  handleSuccessGeolocation(position) {
    const lat = position.coords.latitude;
    const lon = position.coords.longitude;

    DataService.getDataByCoordinates(lat, lon)
      .then((data) => {
        const list = this.state.list;
        list.push(Object.assign(data, { notice: 'Current position' }));
        this.setState({
          isPending: false,
          list,
        }, this.syncList);
      });
  }

  handleFailedGeolocation(error) {
    this.setState({ isPending: false });
  }

  componentDidMount() {
    const list = CacheService.getList();

    if (list && list.length > 0) {
      this.setState({ list });
    } else {
      this.setListFromCooridnates();
    }
  }

  render() {
    return (
      <Layout>
        <Grid topbar={
          <Header />
        } content={
          <List items={this.state.list} onRemove={this.handleRemoveClick} />
        } bottombar={
          this.state.isPending ? (
            <Spinner />
          ) : this.state.list.length < config.limit && (
            <Form onSubmit={this.handleFormSubmit} onChange={this.handleFormChange} value={this.state.input} />
          )
        }/>
      </Layout>
    );
  }

}

export default App;
