import config from '../config';

class DataService {
  constructor({ url, key }) {
    this.apiUrl = url;
    this.apiKey = key;
  }

  get baseUrl() {
    return `${this.apiUrl}?APPID=${this.apiKey}&units=metric`;
  }

  doRequest(url) {
    return fetch(url)
      .then((response) => response.json())
      .then((data) => ({
        id: data.id,
        title: data.name,
        value: data.main.temp,
      }))
      .catch(() => null);
  }

  getDataByCityName(name) {
    const url = encodeURI(`${this.baseUrl}&q=${name}`);
    return this.doRequest(url);
  }

  getDataByCoordinates(lat, lon) {
    const url = encodeURI(`${this.baseUrl}&lat=${lat}&lon=${lon}`);
    return this.doRequest(url);
  }
}

export default new DataService(config.api);
