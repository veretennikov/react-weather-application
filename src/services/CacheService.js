const CacheService = {
  setList(list) {
    const data = JSON.stringify(list);
    localStorage.setItem('list', data);
  },

  getList() {
    const data = localStorage.getItem('list');
    return JSON.parse(data);
  }
};

export default CacheService;
