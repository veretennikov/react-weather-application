# React Weather Application
- add/remove cities
- autodetect position with Geolocation API
- autosave list of cities

## Demo
Demo is working without Geolocation API because HTTP :(
[Surge](http://react-weather-application.surge.sh/)

## Technologies
- React.js

## Global dependencies
- Yarn

## Install
- clone repository
- go inside directory with repository
- run command `yarn` inside

## Start
- run command `yarn start` inside directory with repository
- go to `http://localhost:3000` (see terminal for correct host)
- use Firefox Browser instead Google Chrome for normal working

## Tests
- run command `yarn test` inside directory with repository
